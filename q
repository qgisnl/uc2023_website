[1mdiff --cc _data/speakers.yml[m
[1mindex 69b63de,ca42aab..0000000[m
[1m--- a/_data/speakers.yml[m
[1m+++ b/_data/speakers.yml[m
[36m@@@ -523,3 -523,61 +523,64 @@@[m
    bio: "(Geo)data Analyst at 510, Netherlands Red Cross. Master degree in Geographical Information Management & Application (GIMA) from Utrecht University. Professional achievement - has made maps for the Red Cross operations during the offset of Ukraine crisis (using QGIS of course!). "[m
    social:[m
    - {name: "linkedin", link: "https://www.linkedin.com/in/a-savchuk/"}[m
[32m++<<<<<<< HEAD[m
[32m++=======[m
[32m+ -[m
[32m+   id: 175[m
[32m+   name: "Tammo Jan"[m
[32m+   surname: "Dijkema"[m
[32m+   company: "ASTRON Netherlands Institute for Radio Astronomy"[m
[32m+   thumbnailUrl: "tammo_jan_dijkema.jpeg"[m
[32m+   bio: "Scientific Software Engineer at ASTRON, with a PhD in Mathematics / Numerical Analysis. At ASTRON, my work consists of many aspects of the LOFAR radio telescope, ranging from geodesy for site selection to calibration (in C++ and Python) of the generated data."[m
[32m+   social:[m
[32m+   - {name: "linkedin", link: "https://nl.linkedin.com/in/tammojan/nl"}[m
[32m+   #- {name: "unknown", link: "https://social.edu.nl/@tammojan"}[m
[32m+ -[m
[32m+   id: 176[m
[32m+   name: "Caroline"[m
[32m+   surname: "Robinson"[m
[32m+   company: "Unite Lead, Executive Board Member, Women+ in Geospatial"[m
[32m+   thumbnailUrl: "caroline_robinson.jpeg"[m
[32m+   bio: "Caroline is Founder and Lead Cartographer at Clear Mapping Co. She has been in defence, government, transport, energy, planning and environment, advocating for Diversity, Equity and Inclusion (DE&I) in the geospatial sector.Caroline is a Journalist for Maplines, the membership magazine, for the British Cartographic Society. She is a member and Middle Manager with the Chartered Management Institute. Caroline is a Fellow and Chartered Geographer with the Royal Geographical Society. She has a keen interest in sustainability, resilience and supporting the United Nations’ Sustainable Development Goals (SDGs)."[m
[32m+   social:[m
[32m+   - {name: "linkedin", link: "https://www.linkedin.com/in/caroline-robinson-560a1b35/"}[m
[32m+ -[m
[32m+   id: 178[m
[32m+   name: "Robert"[m
[32m+   surname: "Coup"[m
[32m+   company: "Head of Technology at Koordinates"[m
[32m+   thumbnailUrl: "robert_coup.jpeg"[m
[32m+   bio: "Robert Coup is the head of technology of Koordinates and has been involved in geospatial for nearly twenty years. As well as Kart, he is also a contributor to GDAL and several other open source projects."[m
[32m+   social:[m
[32m+   - {name: "linkedin", link: "https://www.linkedin.com/in/caroline-robinson-560a1b35/"}[m
[32m+   - {name: "github", link: "https://github.com/rcoup"}[m
[32m+   - {name: "twitter", link: "https://twitter.com/amatix/"}[m
[32m+   # - {name: "unknown", link: "https://@rcoup@mastodon.social"}[m
[32m+ -[m
[32m+   id: 179[m
[32m+   name: "Blake"[m
[32m+   surname: "Esselstyn"[m
[32m+   company: "Consulting geographer and demographer"[m
[32m+   thumbnailUrl: "blake_esselstyn.jpeg"[m
[32m+   bio: "A geographer and demographer focused on principled, nonpartisan redistricting. Combines a love of geeking out over geographic data with an ability to synthesize and communicate the stories those data reveal."[m
[32m+   social:[m
[32m+   - {name: "linkedin", link: "https://www.linkedin.com/in/blake-esselstyn-201a39102/"}[m
[32m+ -[m
[32m+   id: 180[m
[32m+   name: "Berit"[m
[32m+   surname: "Mohr"[m
[32m+   company: "GFA Consulting Group GmbH, GIS Consultant"[m
[32m+   thumbnailUrl: "berit_mohr.jpeg"[m
[32m+   bio: "Berit is a GIS Consultant within the Digital Innovation Unit at the GFA Consulting Group GmbH in Hamburg. GFA is a company working in the development cooperation sector. Donors are typically governments, development banks etc. Berit has a background in Geology, Environmental Science and GIS and Remote Sensing. She has worked across sectors including Academia, Local government, international government and start-ups. Her passion is in using GIS and EO to make data visible and understandable for everyone. In her spare time Berit loves to be out hiking, in the garden and be part of the natural environment."[m
[32m+   social:[m
[32m+   - {name: "linkedin", link: "https://www.linkedin.com/in/berit-mohr-414527126/"}[m
[32m+ -[m
[32m+   id: 181[m
[32m+   name: "Mike"[m
[32m+   surname: "Vogt"[m
[32m+   company: "Fraunhofer IEE"[m
[32m+   thumbnailUrl: "mike_vogt.jpg"[m
[32m+   bio: "Mike Vogt, studied physics at the university göttingen and is since then employed at the Fraunhofer IEE. Major topics are grid calculations, optimal powerflow, ai methods for grid operations and grid planning."[m
[32m+   social:[m
[32m+   - {name: "linkedin", link: "https://www.linkedin.com/in/mike-vogt-330326258/"}[m
[32m++>>>>>>> 18b53b2 (fix space in yml)[m
